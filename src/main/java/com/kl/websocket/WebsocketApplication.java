package com.kl.websocket;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Date;
import java.text.DateFormat;

@SpringBootApplication
@EnableScheduling
@RestController
public class WebsocketApplication {
    private Logger logger = LoggerFactory.getLogger(WebsocketApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(WebsocketApplication.class, args);
    }

    int info = 1;

//    @Scheduled(fixedRate = 1000)
//    public void outputLogger() {
//        logger.info("测试日志输出" + info++);
//        throw new RuntimeException();
//    }


    @Autowired
    private ApplicationContext applicationContext;

    @RequestMapping("/hello")
    public String hello() {
        logger.info("访问了hello");
        LoggerMessage loggerMessage = new LoggerMessage();
        applicationContext.publishEvent(loggerMessage);
        return "hello!";
    }
}
